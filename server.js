var express = require('express')

var bodyParserN = require('body-parser')
function bodyParserFunction() {
    var bodyParser = require('body-parser').json()
    return bodyParser
}

var app = express()

var http = require('http').Server(app)
var io = require('socket.io')(http)

app.use(express.static(__dirname))
app.use(bodyParserFunction())
app.use(bodyParserN.urlencoded({extended: false}))
// messages
var messages = [
    // {"name": 'Maruf',"id": 123},
    // {"name": 'Kodu',"id": 321}
];
app.get('/messages',(req,res) =>{
    res.send(messages)
})

// post method
app.post('/messages', (req, res) =>{
    console.log(req.body)
    messages.push(req.body)
    io.emit('message', req.body)
    res.sendStatus(200)
    
    
})
// socket connection 
io.on('connection', (socket)=>{
    console.log('a socket user connected')
})
var server = http.listen(3000, () =>{
    console.log('server listen port is ', server.address().port)
})

